# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
"""
URLconf for registration and activation, using django-registration's
default backend.

If the default behavior of these views is acceptable to you, simply
use a line like this in your root URLconf to set up the default URLs
for registration::

    (r'^accounts/', include('registration.backends.default.urls')),

This will also automatically set up the views in
``django.contrib.auth`` at sensible default locations.

If you'd like to customize the behavior (e.g., by passing extra
arguments to the various views) or split up the URLs, feel free to set
up your own URL patterns for these views instead.

"""


from django.conf.urls import *
from django.views.generic import TemplateView

from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView



from registration.views import activate
from registration.views import register

import django.contrib.auth.views

from accounts import forms
import accounts.views as wv
from accounts.views import login_view, profile_view, logout_view
from wioframework import utilviews
import accounts.views


def enable_ssl(f):
    f.SSL = 1
    return f


urlpatterns = patterns('',
                       url(r'^activate/complete/$',
                           TemplateView.as_view(
                               template_name='registration/activation_complete.html'),
                           name='registration_activation_complete'),
                       # Activation keys get matched by \w+ instead of the more specific
                       # [a-fA-F0-9]{40} because a bad activation key should still get to the view;
                       # that way it can return a sensible "invalid key" message instead of a
                       # confusing 404.
                       url(r'^activate/(?P<activation_key>\w+)/$',
                           wv.register_user,
                           # Use our activation function instead
                           #{'backend': 'registration.backends.default.DefaultBackend'},
                           {'backend':
                               'accounts.backend.DefaultBackend'},
                           name='registration_activate'),
                       url(r'^register/$',
                           enable_ssl(register),
                           {'backend': 'accounts.backend.DefaultBackend',
                            'form_class': forms.RegistrationForm
                            },
                           name='registration_register'),
                       url(r'^login/$',
                           enable_ssl(login_view),
                           name='login_view'),
                       url(r'^logout/$',
                           logout_view,
                           name='logout_view'),
                       url(r'^profile/$',
                           profile_view,
                           name='profile_view'),
                       url(r'^email_update/$',
                           wv.email_update,
                           name='email_update'),
                       url(r'^set_user_scientific/$',
                           wv.set_user_scientific,
                           name='set_user_business'),
                       url(r'^set_user_business/$',
                           wv.set_user_business,
                           name='set_user_business'),
                       url(r'^register/complete/$',
                           RedirectView.as_view(url=reverse_lazy('home')),
                           name='registration_complete'),

                       url(r'^register/closed/$',
                           TemplateView.as_view(
                               template_name='registration/registration_closed.html'),
                           name='registration_disallowed'),
                      url(r'^password_change/$', enable_ssl(django.contrib.auth.views.password_change), name='password_change'),
                      url(r'^password_change/done/$', enable_ssl(django.contrib.auth.views.password_change_done), name='password_change_done'),
                      url(r'^password_reset/$', enable_ssl(django.contrib.auth.views.password_reset), name='password_reset'),
                      url(r'^password_reset/done/$', enable_ssl(django.contrib.auth.views.password_reset_done), name='password_reset_done'),
                      url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                            enable_ssl(django.contrib.auth.views.password_reset_confirm),
                                    name='password_reset_confirm'),
                                        url(r'^reset/done/$', enable_ssl(django.contrib.auth.views.password_reset_complete), name='password_reset_complete'),
                                        
                       (r'', include('registration.auth_urls')),
                       *utilviews.autourls(accounts.views)
                       )
