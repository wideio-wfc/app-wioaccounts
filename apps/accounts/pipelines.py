# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import re
from accounts.models import UserAccount
from django.db.models.signals import post_save
from django.db import models
from social_auth.backends.facebook import FacebookBackend
from social_auth.backends import google
from social_auth.backends.twitter import TwitterBackend
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
import urllib2

from social_auth.models import UserSocialAuth


def register_user(backend, details, response, uid, username, user=None, *args,
                  **kwargs):
    """Create user. Depends on get_username pipeline."""
    if user:
        return {'user': user}
    if not username:
        return None

    # Avoid hitting field max length
    email = details.get('email')
    original_email = None
    if email and UserSocialAuth.email_max_length() < len(email):
        original_email = email
        email = ''

    user, created = UserAccount.objects.get_or_create(
        username=username, email=email)
    return {
        'user': user,
        'original_email': original_email,
        'is_new': True
    }


def add_avatar(backend, details, response, social_user, uid,
               user, *args, **kwargs):
    if not user.picture or not len(str(user.picture)):
        url = None
        if getattr(backend, 'name', None) == 'facebook':
            url = "http://graph.facebook.com/%s/picture?type=large" % response[
                'id']
        elif getattr(backend, 'name', None) == 'twitter':
            url = response["profile_image_url"]
            url = url.replace('_normal', '')
        elif getattr(backend, 'name', None) == 'google-oauth2' and "picture" in response:
            url = response["picture"]
        elif getattr(backend, 'name', None) == 'linkedin':
            pass
        if url:
            #avatar = NamedTemporaryFile(delete=True)
            #avatar.write(urllib2.urlopen(url).read())
            #avatar.flush()
            #avatar.seek(0)
            #user.picture.save(user.username + ".jpg", File(avatar))
            from accounts.models import imageFromUrl
            user.picture = imageFromUrl(url)
            user.save()

from social_auth.backends.pipeline.user import _ignore_field


def update_user_details(backend, details, response, user=None, is_new=False,
                        *args, **kwargs):
    """Update user details using data from provider."""
    if user is None:
        return

    changed = False  # flag to track changes

    for name, value in details.iteritems():
        # do not update username, it was already generated, do not update
        # configured fields if user already existed
        if not _ignore_field(name, is_new):
            if value and value != getattr(user, name, None):
                setattr(user, name, value)
                changed = True
        # Linkedin special business informations
    if backend.name == 'linkedin':
        if not user.display_name:
            user.display_name = user.__unicode__()
        if not user.is_active:
            user.is_active = True

        from network.socialaccounts.SAlinked import LinkedInAccount
        LinkedInAccount.on_sync(user, response)

    if changed:
        user.save()
