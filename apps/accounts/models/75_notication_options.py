#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *

@wideio_publishable()
@wideiomodel
class NotificationOptions(models.Model):
    """
    Allows a user to specify which type of notification it reveives from the website
    """
    user_profile = models.OneToOneField('UserAccount', related_name="notification_options")

    notify_on_new_follower = models.BooleanField(
        default=True,
        help_text="Send me an email when I have a new follower.")
    notify_on_new_comment = models.BooleanField(
        default=True,
        help_text="Send me an email when a comment is added to one of the discussion that I follow.")
    notify_on_new_moderation = models.BooleanField(
        default=True,
        help_text="Send me an email when one of my contribution is accepted.")
    other_email_notifications = models.BooleanField(
        default=True,
        help_text="Accept to receive email notifications when other events happen on the website.")
    product_updates_notifications = models.BooleanField(
        default=True,
        help_text="Receive email when new features are added or removed.")
    commercial_offers_notifications = models.BooleanField(
        default=True,
        help_text="Receive email about commercial offers.")
    legal_notifications = models.BooleanField(
        default=True,
        help_text="Receive email about terms and condition changes.")

    def get_all_references(self):
        return []

    def can_view(self, request):
        return request.user == self.user_profile

    def can_update(self, request):
        return request.user == self.user_profile

    def can_delete(self, request):
        return False


    @staticmethod
    def can_list(request):
        return False

    @staticmethod
    def can_add(request):
        return request.user.is_staff or request.user.notification_options is None

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
        icon = "icon-wrench"
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING = True
        CAN_TRANSFER=False
        form_exclude = ['user_profile']

        MOCKS = {
            "default": [
                {
                 "@id": "user0000-notification-options",
                 "user_profile": "user-0000",
                }
            ]
        }

    @staticmethod
    def _on_load_postprocess(M):
        def get_notification_options(self):
            try:
                return self.notification_options
            except:
                # Apparently we had lost the profile ... recreate it
                sp = NotificationOptions()
                sp.user_profile = self
                sp.save()
                return self.notification_options
        UserAccount = filter(lambda m: m.__name__ == 'UserAccount', M)[0]
        UserAccount.get_notification_options = get_notification_options
