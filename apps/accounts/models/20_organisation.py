#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *
from django_countries.fields import CountryField

# updated 30/07/2013
INDUSTRY_CHOICES = (
    (47, 'Accounting'),
    (94, 'Airlines/Aviation '),
    (120, 'Alternative Dispute Resolution'),
    (125, 'Alternative Medicine'),
    (127, 'Animation'),
    (19, 'Apparel & Fashion'),
    (50, 'Architecture & Planning'),
    (111, 'Arts and Crafts'),
    (53, 'Automotive'),
    (52, 'Aviation & Aerospace'),
    (41, 'Banking'),
    (12, 'Biotechnology'),
    (36, 'Broadcast Media'),
    (49, 'Building Materials'),
    (138, 'Business Supplies and Equipment'),
    (129, 'Capital Markets'),
    (54, 'Chemicals'),
    (90, 'Civic & Social Organization'),
    (51, 'Civil Engineering'),
    (128, 'Commercial Real Estate'),
    (118, 'Computer & Network Security'),
    (109, 'Computer Games'),
    (3, 'Computer Hardware'),
    (5, 'Computer Networking'),
    (4, 'Computer Software'),
    (48, 'Construction'),
    (24, 'Consumer Electronics'),
    (25, 'Consumer Goods'),
    (91, 'Consumer Services'),
    (18, 'Cosmetics'),
    (65, 'Dairy'),
    (1, 'Defense & Space'),
    (99, 'Design'),
    (69, 'Education Management'),
    (132, 'E-Learning'),
    (112, 'Electrical/Electronic Manufacturing'),
    (28, 'Entertainment'),
    (86, 'Environmental Services'),
    (110, 'Events Services'),
    (76, 'Executive Office'),
    (122, 'Facilities Services'),
    (63, 'Farming'),
    (43, 'Financial Services'),
    (38, 'Fine Art'),
    (66, 'Fishery'),
    (34, 'Food & Beverages'),
    (23, 'Food Production'),
    (101, 'Fund-Raising'),
    (26, 'Furniture'),
    (29, 'Gambling & Casinos'),
    (145, 'Glass, Ceramics & Concrete'),
    (75, 'Government Administration'),
    (148, 'Government Relations'),
    (140, 'Graphic Design'),
    (124, 'Health, Wellness and Fitness'),
    (68, 'Higher Education'),
    (14, 'Hospital & Health Care'),
    (31, 'Hospitality'),
    (137, 'Human Resources'),
    (134, 'Import and Export'),
    (88, 'Individual & Family Services'),
    (147, 'Industrial Automation'),
    (84, 'Information Services'),
    (96, 'Information Technology and Services'),
    (42, 'Insurance'),
    (74, 'International Affairs'),
    (141, 'International Trade and Development'),
    (6, 'Internet'),
    (45, 'Investment Banking'),
    (46, 'Investment Management'),
    (73, 'Judiciary'),
    (77, 'Law Enforcement'),
    (9, 'Law Practice'),
    (10, 'Legal Services'),
    (72, 'Legislative Office'),
    (30, 'Leisure, Travel & Tourism'),
    (85, 'Libraries'),
    (116, 'Logistics and Supply Chain'),
    (143, 'Luxury Goods & Jewelry'),
    (55, 'Machinery'),
    (11, 'Management Consulting'),
    (95, 'Maritime'),
    (97, 'Market Research'),
    (80, 'Marketing and Advertising'),
    (135, 'Mechanical or Industrial Engineering'),
    (126, 'Media Production'),
    (17, 'Medical Devices'),
    (13, 'Medical Practice'),
    (139, 'Mental Health Care'),
    (71, 'Military'),
    (56, 'Mining & Metals'),
    (35, 'Motion Pictures and Film'),
    (37, 'Museums and Institutions'),
    (115, 'Music'),
    (114, 'Nanotechnology'),
    (81, 'Newspapers'),
    (100, 'Non-Profit Organization Management'),
    (57, 'Oil & Energy'),
    (113, 'Online Media'),
    (123, 'Outsourcing/Offshoring'),
    (87, 'Package/Freight Delivery'),
    (146, 'Packaging and Containers'),
    (61, 'Paper & Forest Products'),
    (39, 'Performing Arts'),
    (15, 'Pharmaceuticals'),
    (131, 'Philanthropy'),
    (136, 'Photography'),
    (117, 'Plastics'),
    (107, 'Political Organization'),
    (67, 'Primary/Secondary Education'),
    (83, 'Printing'),
    (105, 'Professional Training & Coaching'),
    (102, 'Program Development'),
    (79, 'Public Policy'),
    (98, 'Public Relations and Communications'),
    (78, 'Public Safety'),
    (82, 'Publishing'),
    (62, 'Railroad Manufacture'),
    (64, 'Ranching'),
    (44, 'Real Estate'),
    (40, 'Recreational Facilities and Services'),
    (89, 'Religious Institutions'),
    (144, 'Renewables & Environment'),
    (70, 'Research'),
    (32, 'Restaurants'),
    (27, 'Retail'),
    (121, 'Security and Investigations'),
    (7, 'Semiconductors'),
    (58, 'Shipbuilding'),
    (20, 'Sporting Goods'),
    (33, 'Sports'),
    (104, 'Staffing and Recruiting'),
    (22, 'Supermarkets'),
    (8, 'Telecommunications'),
    (60, 'Textiles'),
    (130, 'Think Tanks'),
    (21, 'Tobacco'),
    (108, 'Translation and Localization'),
    (92, 'Transportation/Trucking/Railroad'),
    (59, 'Utilities'),
    (106, 'Venture Capital & Private Equity'),
    (16, 'Veterinary'),
    (93, 'Warehousing'),
    (133, 'Wholesale'),
    (142, 'Wine and Spirits'),
    (119, 'Wireless'),
    (103, 'Writing and Editing'),
    (-1, 'N/A')
)



def organisation_oracle(n, field=None):
    """
    This function asks an external service for element that we do not know
    about the an organisation but that we woould consider useful to have
    """
    try:
       from tools import organisation_oracle
    except:
       return {}
   
    oracle = None
    if hasattr(settings, "ORGANISATION_ORACLE"):
        import Pyro4
        oracle = Pyro4.Proxy(settings.ORGANISATION_ORACLE)
        oracle._pyroHmacKey = "wiolink"
        oracle._pyroTimeout = 5
    if oracle == None:
        oracle = organisation_oracle.oracle()
    o = oracle.get(n)
    if field is None:
        return o
    return o[field]


def imageFromUrl(url):
    import hashlib
    import urllib
    from django.core.files.base import ContentFile
    from urlparse import urlparse
    from os.path import splitext, basename
    from cStringIO import StringIO
    if url:
        r = urllib.urlopen(url).read()
        h = hashlib.md5(url).hexdigest()
        new_content = StringIO()
        new_content.write(urllib.urlopen(url).read())
        new_content = ContentFile(new_content.getvalue())
        ni = Image()
        # ni.set_expire(expire=datetime.timedelta(minutes=10)) # make it expire
        ni.orig_url = url
        ni.save()

        disassembled = urlparse(url)
        filename, file_ext = splitext(basename(disassembled.path))
        filename = h
        image_name = filename + file_ext
        ni.image.save(image_name, new_content)
        ni.save()
        print type(ni)
        return ni

    return None


@wideio_publishable()
@wideio_name_invokable(
    {
        'image': lambda n, r: imageFromUrl( organisation_oracle(n).get('logo', None)), 
        'website': lambda n, r: organisation_oracle(n).get('website', None), 
        'name': lambda n, r: organisation_oracle(n).get('name', n)
    }, )
@wideio_owned()
# TODO: <wideio_moderated instantiates the decorated class twice therefore not being compatible with wideio_owned appearing below
@wideio_moderated()
# @wideio__taggeable
# @wideio_followable()
# @wideio_likeable()
@wideio_setnames("organisation")
@wideiomodel
class Organisation(models.Model):
    """
    Organisation are legal entities to which users may belong.

    An organisation is initially informal until it is claimed by an individual
    or an entity meant to manage the organisation.
    """

    # From that point, numerous things  change as members as terms and conditions
    # are in place with the organisation rather than with individual users.

    website = models.URLField(null=True, blank=True)
    name = models.CharField(max_length=80, unique=True)
    image = models.ForeignKey('references.Image', null=True, related_name="organisations_with_logo")
    description = models.TextField()
    domain_name = models.CharField(max_length=80, null=True, blank=True)
    # logo = models.ImageField(upload_to="upload/organisation/logo/", null=True)
    # legal requirements
    country = CountryField(null=True, blank=True, db_index=True)
    banner_image = models.ForeignKey('references.Image', null=True, related_name="organisations_with_banner")

    number_of_employees = models.IntegerField(null=True, choices=((0, "1-10"),
                                                                  (1, "10-50"),
                                                                  (2, "50-250"),
                                                                  (3, "250-1000"),
                                                                  (4, "1000-5000"),
                                                                  (5, "5000+"),
                                                                  ), default=0)

    sector = models.IntegerField(null=True, choices=INDUSTRY_CHOICES, default=-1)

    def __unicode__(self):
        try:
            return unicode(self.name)
        except:
            return unicode(self.name, errors='replace')

    def __str__(self):
        return self.name.encode('utf8')

    def __repr__(self):
        return "[organisation:" + self.name.encode('utf8') + "]"

    def get_all_references(self, stack):
        st = []
        for x in self.lab_set:
            st.append(x)
        return st

    def on_add(self, request):
        if self.sector == None:
            self.sector = -1

    def on_update(self, request):
        if self.domain_name and self.wiostate == 'V':
            for u in UserAccount.objects.filter(wiostate='V'):
                if u.email.split('@') == self.domain_name:
                    u.organisation = self
                    u.save()

    @staticmethod
    def can_list(request):
        return True

    def can_view(self, request):
        return True

    class WIDEIO_Meta:
        icon = "icon-globe"
        mandatory_fields = ["name", "website", "image", "country", "number_of_employees", "sector"]
        form_exclude = ["domain_name"]  # < SET BY STAFF
        permissions = dec.perm_for_logged_users_only
        allow_query = ['organisation_type']
        sort_enabled = ['name']
        search_enabled = 'name'

        class Actions:
            @wideio_action(icon="ion-log-in",
                           possible=lambda x, r: r.user.is_authenticated() and not (
                                   getattr(r.user, 'organisation', None == x)))
            def join_this_organisation(self, r):
                r.user.organisation = self
                r.user.save()
                return "alert('Organisation has been joined');"

            @wideio_action(possible=lambda x, r: r.user.is_authenticated() and r.user.organisation == x)
            def leave_this_organisation(self, r):
                r.user.organisation = None
                r.user.save()
                return "alert('Organisation has been left');"

        MOCKS = {
            'default': [
                {
                    '@id': 'corp0000',
                    'website': 'www.acmecorp.demo',
                    'name': 'ACME Corp',
                    'image': None,
                    'description': 'Leader in its industry',
                    'domain_name': 'acmecorp.demo',
                    'country': 'UK',
                    'banner_image': None,
                    'number_of_employees': 4,
                    'sector': 82
                },
                {
                    '@id': 'camford-univeristy',
                    'website': 'www.camford.ac.demo',
                    'name': 'CamFord University',
                    'image': None,
                    'description': 'Leader in its industry',
                    'domain_name': 'acmecorp.demo',
                    'country': 'UK',
                    'banner_image': None,
                    'number_of_employees': 4,
                    'sector': 82
                }
            ]
        }
