#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import re
from wioframework.amodels import *

SHA1_RE = re.compile('^[a-f0-9]{40}$')

class RegistrationTokenManager(models.Manager):
    """
    Return matching user for the provided token
    """

    def validate_token(self, activation_key):
        if SHA1_RE.search(activation_key):
            try:
                token = self.get(registration_token=activation_key)
                user = token.owner
            except self.model.DoesNotExist:
                return False
            if not token.is_expired():
                user.reg_token.delete()
                user.is_active = True
                user.save()
                return user
            else:
                user.reg_token = RegistrationToken()
                user.reg_token.save()
                user.is_active = False
                print "Token is expired"
                user.save()
                return False
        return False


class RegistrationToken(models.Model):
    objects = RegistrationTokenManager()

    owner = models.ForeignKey('accounts.UserAccount', related_name="reg_tokens", db_index=True)
    send_to = models.EmailField(unique=True, blank=True)
    registration_token = models.CharField(max_length=40, blank=True)
    expires = models.CharField(max_length=40, blank=True)

    def __unicode__(self):
        return unicode(self.registration_token)

    def is_expired(self):
        e = dt.datetime.strptime(self.expires, "%Y-%m-%d %H:%M:%S.%f")
        if datetime.datetime.now() >= e:
            return True
        else:
            return False

    # Here is a user's registration token - to be set upon registration
    def set_token(self, request):
        import hashlib
        import random
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        username = request.user.username
        if isinstance(username, unicode):
            username = username.encode('utf-8')
        new_registration_token = hashlib.sha1(salt + username).hexdigest()
        self.registration_token = new_registration_token

    def set_expiration(self):
        expires_on = dt.datetime.now() + \
                     dt.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS)
        self.expires = expires_on

    def on_add(self, request):
        from django.contrib.sites.models import Site

        if Site._meta.installed:
            current_site = Site.objects.get_current()
        else:
            current_site = RequestSite(request)

        self.set_token(request)
        self.set_expiration()
        self.save()

        c = {'site': current_site, 'token': self}
        from wioframework.utils import wio_send_email
        wio_send_email(request,
                       None,
                       "Validate your account",
                       "mails/email_activation.html",
                       c,
                       [self.send_to])

    def to_json(self):
        return {
            'send_to': self.send_to,
            'registration_token': self.registration_token,
        }
