#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import urllib
import json
from django.contrib.auth.models import AbstractUser
from django.template.loader import render_to_string
from django_countries.fields import CountryField
from django.contrib.auth import logout

from wioframework.amodels import *
from references.models import Image
from wioframework import decorators as dec
from wioframework.multimock import multimock
from accounts.utils import avoid_classic_domains

@wideio_name_invokable({
    "username": (lambda n, r: n[:30]),
    "set_organisation": (lambda n, r: avoid_classic_domains(n.split('@')[1]))
},
    name_field="email"
)
@wideio_history_track(["number_of_times_connected"], length=datetime.timedelta(365), max_update_interval=3600 * 24 * 3)
@wideio_publishable()
@wideiomodel
@wideio_setnames("member")
class UserAccount(AbstractUser):
    # ----------------------------------------------------------------
    # Good To Know
    # ----------------------------------------------------------------
    picture = models.ForeignKey('references.Image', null=True)

    # joined_service_at = models.DateTimeField(auto_now_add=True)
    display_name = models.CharField(
        max_length=255,
        null=True)  # user name / lfirst name (are we still using name)

    tagline = with_advanced_help_text("""
        +1: Simple, clear and honest
        -1: Any boastful self-promotion
        """)(models.TextField(
        max_length=255,
        blank=True,
        default="",
        help_text="Introduce yourself to other users"))
    organisation = models.ForeignKey(
        'accounts.Organisation',
        null=True,
        db_index=True)

    # ----------------------------------------------------------------
    # Good To Know and legals
    # ----------------------------------------------------------------
    date_of_birth = models.DateField(blank=True, null=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        # ('N', 'Non-conforming')
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    nationality = CountryField(null=True, blank=True, db_index=True)
    country_of_residence = CountryField(null=True, blank=True, db_index=True)

    ##
    # BETA USERS HAVE DIFFERENT RIGHTS SOME ARE ALLOWED TO TEST THE LATEST FEATURES AND SOME AREN'T
    ##
    # FIXME: > ALL THESE SHOULD BE IN METADATA
    # ----------------------------------------------------------------
    number_of_times_connected = models.IntegerField(default=0, null=True, blank=True)

    # ----------------------------------------------------------------
    # FIXME: Shall we use inheritance ?
    # FIXME: METHODS BELOW SHOULD BE INJECTED

    def get_home_page(self, request):
        if self.wiostate == 'CP':
            return HttpResponseRedirect('/accounts/password/change/')

        if self.is_not_yet_beta():
            logout(request)
            return "home_not_yet_a_beta_user",

        if (((not self.is_scientist) and (
                not self.is_customer)) or "DECIDE" in request.GET):
            return "home_in_decide"
        else:
            actions = self.get_potential_actions()
            if actions['required']:
                return "home_in_urgent"
            else:
                if self.is_scientist:
                    return "home_in_scientist"
                else:
                    return "home_in_scientist"


    def is_not_yet_beta(self):
        if (settings.LOCKED_BETA_MODE):
            if not (self.is_superuser or self.is_staff):
                return True
        return False

    def get_all_references(self, stack):
        st = []
        if self.is_scientist:
            st.append(self.scientific_profile)
        if self.is_customer:
            st.append(self.business_profile)
        if self.is_investor:
            st.append(self.investor_profile)

        st += list(self.blogentry_rev_author.all())
        st.append(self.get_extended_profile())
        if (stack == []):
            return st
        return st

    # OPTIONAL REGISTRATION TOKEN TO BE ISSUED UPON REGISTRATION
    @property
    def reg_token(self):
        if (self.reg_tokens.count() == 0):
            return None
        # return self.reg_tokens[0]
        g = self.reg_tokens.iterator()
        return g.next()

    def get_website_version(self):
        return self._website_version

    @multimock(
        default={'lib.utils.wio_send_email': '@mock'}
    )
    def invite(self, request):
        if self.is_active and not (self.email.endswith('@wide.io')):
            return

        from lib.utils import wio_send_email
        from django.contrib.auth.tokens import default_token_generator
        from django.utils.http import urlsafe_base64_encode
        password = self.__class__.objects.make_random_password()
        self.set_password(password)
        self.save()

        token = default_token_generator.make_token(self)

        uidb64 = urlsafe_base64_encode(self.pk)
        # FIXME: REF FOR WEBSITE SHOULD BE COMING FROM SETTNG
        site = "http://www.wide.io/" if settings.PRODUCTION_MODE else ("http://" + request.get_host() + "/")
        wio_send_email(
            request,
            consent="",
            subject='Your invite to wide.io',
            template='mails/email_invitation.html',
            context={
                'x': self,
                'password': password,
                'confirm_url': "%saccounts/password/reset/confirm/%s-%s/" % (site, uidb64, token)
            },
            to_users=[self],
            url_notification=None)
        return {'_redirect': request.user.get_view_url()}

    # THE FOLLOW RELATIONSHIP IN-BETWEEN USERS IS BIT DIFFERENT THAN THE OTHER TYPE OF FOLLOW RELATIONSHIP
    followers = models.ManyToManyField(
        'accounts.UserAccount',
        related_name="following",
        blank=True,
        null=True)
    followers_count = models.IntegerField(default=0, blank=True, null=True)

    def get_stripecustomer(self):
        import stripe
        import settings
        stripe.api_key = settings.STRIPE_SECRET_KEY
        return stripe.Customer.retrieve(self.stripecustomer.customer_id)

    def get_organisation(self):
        try:
            return self.organisation
        except:
            # Apparently we had lost the profile ... recreate it
            self.organisation = None
            self.save()
            return self.organisation

    def get_options(self):
        return self.get_notification_options()

    @staticmethod
    def can_list(request):
        return not request.user.is_anonymous()

    @staticmethod
    def can_add(request):
        return request.user.is_staff

    def can_update(obj, request):
        return request.user.is_staff or obj.id == request.user.id

    def can_delete(obj, request):
        return (not obj.is_superuser) and (request.user.is_staff)

    def on_delete(self, request):
        if self.business_profile is not None:
            self.business_profile.delete()
        if self.scientific_profile is not None:
            self.scientific_profile.delete()

    def get_email(self):
        # + "@" + self.email.split('@')[1][0] + "_____." + self.email.split('@')[1].split(".")[-1]
        return self.email.split('@')[0]

    def get_username(self):
        try:
            return self.get_email()
        except:
            return self.username

    def get_picture(self):
        import settings
        if not (self.picture is None):
            pg1 = self.picture.for_scale(128, 128)
            if pg1 != None:
                return settings.MEDIA_URL + pg1

        return settings.MEDIA_URL + "images/default_user.png"

    def set_picture(self, img):
        if type(img) not in [str, unicode]:
            self.picture = img
        else:
            picture = Image()
            picture.name = img
            picture.image = img
            picture.wiostate = 'V'
            picture.save()
            self.picture = picture
        self.save()

    def get_picture24(self):
        import settings
        if not ((self.picture is None)):
            pg1 = self.picture.for_scale(24, 24)
            if pg1 != None:
                return settings.MEDIA_URL + pg1
        return settings.MEDIA_URL + "images/default_user_24.png"

    def get_account(self):
        if self.creditaccount_rev_owner.count() == 0:
            from payment.models import CreditAccount
            ca = CreditAccount()
            ca.owner = self
            ca.credit = 0
            ca.save()
        return self.creditaccount_rev_owner.all()[0]

    def on_add(self, request):
        if not self.display_name:
            self.display_name = self.__unicode__()
        try:
            from backoffice.models import MotOfTheDay
            most_recent_mot = MotOfTheDay.objects.all().order_by('published')[0]
            most_recent_mot.send_to(request, self)
        except:
            pass
        # ensure the user is connected to his organisation based on his email
        if self.email:
            from accounts.models import Organisation
            co = Organisation.objects.filter(domain_name=self.email.split('@')[1], wiostate='V')
            if co.count():
                self.organisation = list(co)[-1]

    def on_db_update(self, request):
        if self.password == None or self.password == "":
            password = self.__class__.objects.make_random_password()
            self.set_password(password)

    def on_update(self, request):
        if self.email:
            from accounts.models import Organisation
            co = Organisation.objects.filter(domain_name=self.email.split('@')[1], wiostate='V')
            if co.count():
                self.organisation = list(co)[-1]

    def __unicode__(self):
        if self.display_name:
            # self.first_name + " " + self.last_name + " " + self.email
            return self.display_name
        elif self.first_name or self.last_name:
            return self.first_name + " " + self.last_name
        elif self.email:
            return self.email.split('@')[0]
        elif self.username:
            return self.username
        else:
            return self.id

    def get_followers(self):
        return self.followers.all()

    def set_organisation(self, name):
        if name is not None:
            from accounts.models import Organisation
            self.organisation = Organisation.invoke_by_name(name)

    def sci_question_rank(self, q):
        cr = 5
        for t in self.teams:
            for ci in range(1, 5):
                if cr > ci:
                    if getattr(t, "associated_question" + i) == q:
                        cr = 1
                        break
        return cr

    def on_view(self, request):
        if self.password == None or self.password == "":
            password = self.__class__.objects.make_random_password()
            self.set_password(password)
            self.save()

        ctx = {}
        view_options = {'default_perpage': 10, 'pager_disable_perpage': 1,
                        'add_enabled': str(1 if request.user == self else 0)}
        ctx["view_opts"] = "$" + json.dumps(view_options)
        view_options['hide_navbar'] = "1"
        ctx["social_view_opts"] = "$" + json.dumps(view_options)
        return ctx



    def default_min_kudos(self):
        return 0

    def get_size(self):
        return self.get_extended_profile().kudos

    class WIDEIO_Meta:
        icon = 'ion-person'
        NO_DRAFT = True
        permissions = dec.perm_read_logged_users_write_for_admin_only
        form_exclude = [
            'user',
            'password',
            'last_login',
            "username",
            "firstname",
            "lastname",
            "is_superuser",
            "is_investor",
            "followers",
            "groups",
            "user_permissions",
            "followers_count",
            "staff_status",
            "active",
            "number_of_times_connected",
            "scientific_profile",
            "investor_profile",
            "business_profile",
            "_has_requested_beta_access",
            "_website_version",
            "options",
            'display_name']
        admin_only = ['is_superuser']
        form_fieldgroups = [('Personal Info',
                             ['first_name', 'last_name', 'gender', 'tagline', 'organisation', 'date_of_birth',
                              'country_of_residence', 'nationality', 'picture']),
                            # ('options', ['options']),
                            ]
        sort_enabled = ['last_login', 'email', 'first_name', 'last_name']
        search_enabled = [
            "first_name",
            "last_name",
            "display_name",
            "username",
            "email",
            "last_login"
        ]
        allow_query = [
            'is_scientist',
            'is_customer',
            'is_staff',
            '_website_version__exact',
            '_website_version__lt',
            '_website_version__gt',
            '_has_requested_beta_access'
        ]
        allow_default_query_override = True

        @staticmethod
        def default_query(request):
            from django.db.models import Q
            if request.user.is_staff:
                return Q(wiostate='V')
            else:
                return Q(wiostate='V', number_of_times_connected__gt=0)
                return Q(followers=request.user, wiostate='V') | Q(following=request.user, wiostate='V')

        MOCKS = {
            'default': [
                {
                    '@id': 'user-0000',
                    'username': 'user0000',
                    'picture': None,
                    # 'joined_service_at':'2015-10-10',
                    'date_joined': '2015-10-10',
                    'last_login': '2015-10-11',
                    'display_name': 'test user',
                    'tagline': 'A test user',
                    'organisation': None,
                    'date_of_birth': '1977-10-11',
                    'gender': 'M',
                    'nationality': 'UK',
                    'country_of_residence': 'FR',
                    'number_of_times_connected': 1,
                    'email': 'test@wide.io',
                    'password': "d73b04b0e696b0945283defa3eee4538"
                }
            ]
        }

        class Gamification:
            @staticmethod
            def require_claimed_identity(user):
                account_url = user.get_base_url() + "/%s/" + user.id + '/'
                if (not user.first_name) or (not user.last_name) or len(user.first_name) < 2:
                    return ["required", "Other people on the website would like to know your name",
                            account_url % 'update']
                if not user.date_of_birth:
                    return ["required", "Thanks to indicate your age", account_url % 'update']
                if not user.country_of_residence:
                    return ["required", 'Thanks to indicate your country of residence', account_url % 'update']

            @staticmethod
            def provide_meaningful_tagline(user):
                account_url = user.get_base_url() + "/%s/" + user.id + '/'
                if not user.tagline:
                    return ["important", 'Please fill in you name information', account_url % 'update']

        class Actions:
            @wideio_action(
                possible=lambda o,
                                r: not (
                        r.user in o.get_followers()),
                icon="icon-plus-sign-alt")
            def follow(self, request):
                rel = self.followers.add(request.user)
                from wioframework.utils import wio_send_email
                wio_send_email(request, "email_on_new_follower",
                               "You have a new follower",
                               "mails/new_follower.html",
                               {'follower': request.user, 'x': self},
                               [self])
                atomic_incr(self, "followers_count", +1)
                return 'alert("ok");'

            @wideio_action(
                possible=lambda o, r: (
                        r.user in o.get_followers()), icon="icon-unlink")
            def unfollow(self, request):
                kwargs = {
                    'from_useraccount_id': self.id,
                    'to_useraccount_id': request.user.id}
                tx = self.followers.through.objects.filter(**kwargs)[0]
                tx.delete()
                atomic_incr(self, "followers_count", -1)
                return 'alert("ok");'

            # FIXME: THESE SHOULD BE INJECTED REL_ACTIONS
            @wideio_action(possible=lambda o,
                                           r: (r.user.is_authenticated()) and (r.user != o),
                           icon="ion-email",
                           mimetype="text/html",
                           xattrs="data-modal-link title=\"Send message\" ")
            def send_message(self, request):
                from network.models import DirectMessage
                return HttpResponseRedirect(
                    DirectMessage.get_add_url() +
                    "?_AJAX=1&to=" +
                    self.id + "&" + urllib.urlencode(request.GET))

            @wideio_action(possible=lambda o,
                                           r: (r.user.is_authenticated()) and (r.user != o),
                           icon="icon-list",
                           mimetype="text/html",
                           xattrs=" title=\"View conversation\" ")
            def view_conversation(self, request):
                from network.models import DirectMessage
                return HttpResponseRedirect(
                    DirectMessage.get_list_url() +
                    "?to=" +
                    self.id + "&_VIEW_MODE=view"
                    + '&_VIEW_OPTIONS={add_enabled:1}')

            @wideio_action(
                possible=lambda o, r: (
                        o._has_requested_beta_access
                        and o._website_version not in (1, 2)
                        and r.user.is_staff
                )
            )
            def allow_access(self, request):
                self._website_version = 1
                self.save()
                from django.core.mail import send_mail
                # FIXME: MOVE LOCATION of allow_access.htmml
                send_mail("New user wants access to wide io (" + self.user + ")",
                          render_to_string(
                              "mails/allow_access.htmml",
                              {}),
                          "system@wide.io",
                          [self.email]
                          )
                return "alert('Done');"

            @wideio_action(
                possible=lambda o, r: (
                        not o.is_staff and r.user.is_superuser))
            def set_staff(self, request):
                self.is_staff = True
                self.save()
                return "alert('Done');"

            @wideio_action(
                possible=lambda o, r: (
                        o.is_staff and r.user.is_superuser))
            def unset_staff(self, request):
                self.is_staff = False
                self.save()
                return "alert('Done');"

            @wideio_action(possible=lambda o, r: r.user.is_staff)
            def invite(self, request):
                profile = self.registrationprofile_set.create_profile(self)
                self.registrationprofile_set.activate_user(profile.activation_key)

                if len(self.email) < 30:
                    self.username = self.email
                else:
                    self.username = self.email.split('@')[0][:30]

                self._website_version = -1
                self.save()

                self.invite(request)
                return "alert('Done');"

