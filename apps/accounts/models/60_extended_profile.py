#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import base64
import random
import time
from wioframework.amodels import *


@wideio_publishable()
@wideio_history_track(["kudos"], length=datetime.timedelta(365), max_update_interval=datetime.timedelta(1))
@wideiomodel
class ExtendedProfile(models.Model):
    """
    Extended profile information that are not modifiable by user api -
    and used to memorise usage of the website by the user.
    """
    user_profile = models.OneToOneField('UserAccount', related_name="extended_profile")

    _has_requested_beta_access = models.BooleanField(default=False)
    _can_select_website_version = models.BooleanField(default=False)
    _website_version = models.IntegerField(default=0)  # -1 stands for latest
    is_verified_scientist = models.BooleanField(default=False)  # < ALLOWS TO ACCESS RESEARCHER DISCOUNT PRICE

    kudos = models.IntegerField(default=0, help_text="Community points")
    api_key = models.CharField(max_length=255, default=lambda: base64.b64encode(hashlib.sha256(
        hashlib.md5(str(random.random() + time.time())).hexdigest() + "AAA" + str(time.time())).digest()))

    # FIXME: INFORMATION BELOW SHOULD NOT BE THERE
    homepage = models.URLField(
        max_length=255,
        blank=True,
        null=True,
        help_text="The website which describes you best")

    biography = models.TextField(
        blank=True,
        max_length=2000,
        help_text='Self introduction')
    team = models.ForeignKey('accounts.Team', null=True, db_index=True)

    def increase_kudos(self, q):
        # FIXMNE: USE ATOMICS
        self.kudos += q
        self.save()

    def decrease_kudos(self, q):
        # FIXMNE: USE ATOMICS
        self.kudos -= q
        self.save()

    def get_all_references(self, stack):
        st = []
        return st

    ##
    # basic permissions
    ##
    def can_view(self, request):
        return True

    def can_delete(self, request):
        return False

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    @staticmethod
    def can_add(request):
        return False

    def can_update(self, request):
        return request.user.is_staff or (request.user.extended_profile_id == self.id)

    class WIDEIO_Meta:
        DISABLE_VIEW_ACCOUNTING = True
        NO_DRAFT = True
        CAN_TRANSFER = False
        permissions = dec.perm_read_logged_users_write_for_admin_only
        icon = "icon-superscript"
        form_exclude = ['kudos', 'api_key', 'team', 'extended_profile']

        class Actions:
            @wideio_action(icon="ion-refresh", possible=lambda o, r: True)
            def regenerate_api_key(self, request):
                self.api_key = base64.b64encode(hashlib.sha256(
                    hashlib.md5(str(random.random() + time.time())).hexdigest() + "AAA" + str(time.time())).digest())
                self.save()
                return "alert('ok')"

            @wideio_action(icon="icon-cogs", possible=lambda o, r: r.user.is_staff)
            def plus_10_kudos(self, request):
                self.kudos = self.kudos + 10
                self.save()
                return "alert('ok')"

            @wideio_action(icon="icon-cogs", possible=lambda o, r: r.user.is_staff)
            def plus_100_kudos(self, request):
                self.kudos = self.kudos + 100
                self.save()
                return "alert('ok')"

            @wideio_action(icon="icon-cogs", possible=lambda o, r: r.user.is_staff)
            def plus_1000_kudos(self, request):
                self.kudos = self.kudos + 1000
                self.save()
                return "alert('ok')"

        MOCKS = {
            'default': [
                {
                    'user_profile': 'user0000'
                }
            ]
        }

    @staticmethod
    def _on_load_postprocess(M):
        def get_extended_profile(self):
            try:
                return self.extended_profile
            except:
                # Apparently we had lost the profile ... recreate it
                sp = ExtendedProfile()
                sp.user_profile = self
                sp.save()
                return self.extended_profile

        UserAccount = filter(lambda m: m.__name__ == 'UserAccount', M)[0]
        UserAccount.get_extended_profile = get_extended_profile
