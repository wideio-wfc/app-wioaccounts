#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *


@wideio_owned()
@wideiomodel
class UserInvitation(models.Model):
    first_name = models.CharField(max_length=32, blank=True)
    last_name = models.CharField(max_length=32, blank=True)
    email = models.EmailField()
    useraccount = models.ForeignKey('accounts.UserAccount', default=None, null=True)

    def __unicode__(self):
        return unicode(" ".join([self.first_name, self.last_name]))

    def on_add(self, request):
        ## CHECK THAT THEY ARE NOT YET ON THE SYSTEM AND/OR THAT THEY HAVEN'T BEEN INVITED TOO RECENTLY...
        from django.contrib import messages

        if UserAccount.objects.filter(email=self.email).count():
            self.delete()
            messages.add_message("A user with this email address already exists in our database.")
            return {"_redirect": "/"}

        ## TODO A DEACTIVATED USER WITH FIRSTNAME, LAST NAME EMAIL
        uname = self.email.split('@')[0]
        cu = UserAccount(is_active=False, username=uname, email=self.email, first_name=self.first_name,
                         last_name=self.last_name)
        cu.wiostate = 'V'
        cu.on_add(request)
        cu.save()

        ## SEND HIM INIVTE USING THE ACTION + A TWEAK SO THAT USER RECEIVE BONUS ON JOIN
        self.useraccount = cu
        self.save()

        cu.invite(request)
        messages.add_message(request, messages.INFO, '<i class="icon-envelope"></i> Invitation sent')
        return {"_redirect": "/accounts/profile/"}

    def get_all_references(self):
        return []

    def can_view(self, request):
        return True

    def can_update(self, request):
        return False

    def can_delete(self, request):
        return False

    @staticmethod
    def can_add(request):
        return request.user.is_staff

    @staticmethod
    def can_list(request):
        return request.user.is_staff

    class WIDEIO_Meta:
        NO_DRAFT = True
        CAN_TRANSFER = False
        # permissions = dec.perm_read_logged_users_write_for_admin_only
        form_exclude = ['useraccount']
        icon = "icon-wrench"
        MOCKS = {
            'default': [
                {
                    'first_name': 'John',
                    'last_name': 'Smith',
                    'email': 'john.smith@gmail.com',
                    'useraccount': None,
                    'owner': 'user-0000'
                }
            ]
        }
