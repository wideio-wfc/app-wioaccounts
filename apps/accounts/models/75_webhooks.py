#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *
from wioframework import gen2

##
## Allow user to register webhooks to our application
##

#@wideiomodel
#class WebHook(models.Model):
#   can be an instance or a model ?
#   content_type = models.ForeignKey(
#        ContentType,
#        null=True,
#        verbose_name='content type',
#        related_name="content_type_set_for_%(class)s")
#   object_pk = models.TextField('object ID', null=True)
#   content_object = gen2.GenericForeignKey2(
#        ct_field="content_type",
#        fk_field="object_pk")
#   event_type # on_update...
#   url=models.UrlField()
#MODELS += [WebHook]
#MODELS += get_dependent_models(WebHook)

def call_web_hook_for(oid, payload, _class,event, owner):
    # Look for webhook matching current object ?
    # None on one fthe fiels means all
    # RESPECT PRIVACY WEB_HOOK ARE NOT CALLED CROSS USERS ?
    # Unless we talk about shared object ()
    req0=WebHook.objects.filter(object_pk=None)
    req0=WebHook.objects.filter(object_pk=oid)
    req1=WebHook.objects.filter(event=None) 
    req1=WebHook.objects.filter(event=event)
    req2=WebHook.objects.filter(event=None)
    req2=WebHook.objects.filter(event=event)
    for wh in webhooks:
        webhook.call(payload)
        