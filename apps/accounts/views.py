# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from datetime import datetime, time
import json

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.contrib.sites.models import Site, RequestSite
from django import forms
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.shortcuts import render_to_response, RequestContext
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt

from registration import signals

from network.models import Announcement
from accounts.models import RegistrationTokenManager, RegistrationToken

from wioframework.decorators import login_required
from wioframework.jsonresponse import JsonResponse
import settings
from django.views.decorators.debug import sensitive_post_parameters
from functools import reduce


@csrf_exempt
@sensitive_post_parameters('password')
@never_cache
def login_view(request):
    template_file = 'registration/login.html',
    redirect_field_name = REDIRECT_FIELD_NAME
    redirect_to = request.REQUEST.get(redirect_field_name, '')
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            print "\n:: :: form is valid\n"
            if not request.session.test_cookie_worked() and not request.REQUEST.get("_WITH_TEST_COOKIE",""):
                request.session.set_test_cookie()
                r = HttpResponseRedirect("/accounts/login/?_WITH_TEST_COOKIE=1")
                r.status_code = 307
                return r
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL
            # Additional check for our custom registration tokens
            user = form.get_user()
            if (not user.is_staff and not user.is_superuser) and user.reg_token is not None:
                # print "I shouldn't be here if I have a token, no?"
                if user.reg_token.is_expired():
                    # Issue new token
                    reg_token = RegistrationToken(
                        send_to=user.email,
                        owner=user)
                    reg_token.save()
                    user.is_active = False
                    user.save()
                    messages.add_message(
                        request,
                        messages.WARNING,
                        'Your account is currently disabled. You can reactivate your account through the activation link sent to the email address registered with your account.')
                    return HttpResponseRedirect("/")
                else:
                    messages.add_message(
                        request,
                        messages.WARNING,
                        'Your account is still awaiting proper activation. You can do so by clicking the activation link sent to the email address registered with your account.')
            login(request, form.get_user())
            if (request.user):
                request.user.number_of_times_connected = request.user.number_of_times_connected + \
                    1
                request.user.save()
            if (request.user.is_superuser) and (not request.user.is_staff):
                request.user.is_staff = True
                request.user.save()
            if request.REQUEST.get("_WITH_TEST_COOKIE", ""):
                if request.session.test_cookie_worked():
                    request.session.delete_test_cookie()
                else:
                    messages.add_message(
                        request,
                        messages.WARNING,
                        'This site is using cookies. Login requires cookies to be enabled.')
            context = {
                'request_user': request.user.username,
                'request_user_id': request.user.id,
                'redirect': redirect_to}
            if ("_JSON" in request.REQUEST):
                return JsonResponse(context)
            else:
                # render_to_response(template_file, context,context_instance=RequestContext(request))
                return HttpResponseRedirect(redirect_to)
        else:
            print "\n:: form is not valid"
            user = form.get_user()
            if hasattr(user, "is_active") and not user.is_active:
                print "  :: user `%s` is not active" % request.POST.get('username')
                messages.add_message(
                    request,
                    messages.WARNING,
                    'Account \'%s\' is inactive' %
                    request.POST.get('username'))
            else:
                messages.add_message(
                    request,
                    messages.WARNING,
                    'Submitted details failed to validate')
    else:
        form = AuthenticationForm(request)
    request.session.set_test_cookie()
    if Site._meta.installed:
        try:
          current_site = Site.objects.get_current()
        except:
          current_site = RequestSite(request)
    else:
        current_site = RequestSite(request)
    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site_name': current_site.name,
        'request': request}
    # context.update( csrf(request) ) ## we use the cookies
    if ("_JSON" in request.REQUEST):
        return JsonResponse(context)
    else:
        return render_to_response(
            template_file,
            context,
            context_instance=RequestContext(request))
login_view.SSL = True


@never_cache
@login_required
def profile_view(request):
    template_file = "registration/profile.html"
    comments = Announcement.objects.filter(
        author_id=request.user.id).order_by('-created_at')[:5]
    context = {
        'request': request,
        "settings": settings,
        "comments": comments,
        "sr1_url": request.user.get_view_url(),
        "sr1_context": {'_AJAX': 1, 'oid': request.user.id}
    }
    return render_to_response(
        template_file,
        context,
        context_instance=RequestContext(request))


@never_cache
@login_required
def email_update(request):
    from wioframework import widgets

    class ChangeEmailForm(forms.Form):
        email = forms.EmailField(
            widget=widgets.WIOCharFieldWidget(
                maxlength=75,
                default=request.user.email),
            required=True)

        def __init__(self, user, *args, **kwargs):
            self.user = user
            super(ChangeEmailForm, self).__init__(*args, **kwargs)

    success = False
    errors = ''
    if request.method == 'POST':
        form = ChangeEmailForm(request.user, data=request.POST)
        if form.is_valid():
            request.user.email = form['email'].value()
            request.user.save()
            success = True
        else:
            errors = form.errors['email']

    context = {
        'request': request,
        'email_form': ChangeEmailForm(user=request.user),
        'settings': settings,
        'errors': errors,
        'success': success,
    }
    return render_to_response(
        "registration/email_change.html", # FIXME: ensure directory correct
        context,
        context_instance=RequestContext(request))


@never_cache
@login_required
def set_user_scientific(request):
    request.user.is_scientist = True
    request.user.is_customer = False
    request.user.save()
    return HttpResponseRedirect("/")


@never_cache
@login_required
def set_user_business(request):
    request.user.is_customer = True
    request.user.is_scientist = False
    request.user.save()
    return HttpResponseRedirect("/")


@never_cache
def register_user(request, activation_key, **kwargs):
    user = RegistrationToken.objects.validate_token(activation_key)
    backend = kwargs['backend']
    print "enter register_user", backend, user
    if user and backend:
        messages.add_message(
            request,
            messages.INFO,
            'Your account has been successfully activated and you have been automatically logged in')
        setattr(user, 'backend', backend)
        login(request, user)
        return HttpResponseRedirect("/")
    else:
        messages.add_message(
            request,
            messages.WARNING,
            'The activation period for that link has expired. You can reactivate your account through the activation link sent to the email address registered with your account.')
        return HttpResponseRedirect("/")


@never_cache
def logout_view(request):
    from django.contrib.auth import logout
    logout(request)
    messages.add_message(
        request,
        messages.INFO,
        'You have been successfully logged out.')
    return HttpResponseRedirect("/")


from wioframework import utilviews as uv
from accounts import models

VIEWS = reduce(lambda x, y: x + uv.AUDLV(y), models.MODELS, [])
