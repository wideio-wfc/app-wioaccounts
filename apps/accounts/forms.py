# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import django.forms as djf
from registration import forms
from wioframework.captcha import fields as cfields
from accounts import models
import settings

if settings.PRODUCTION_MODE:
    class ContactForm(djf.ModelForm):
        email_address = djf.EmailField()
        # topic=djf.CharField(choices=[("HR","Recruitement"),("HELP","Help"),("PARTNERSHIP","Partnership")])
        # body=djf.TextField("")
        discount_code = djf.CharField(max_length=32, required=False)
        captcha = cfields.ReCaptchaField(
            attrs={'theme': 'clean'},
            use_ssl=True)


class RegistrationForm(forms.RegistrationFormNoUsername):
    # user_type = djf.ChoiceField(choices=[("S","Scientist"),("B","Business")])
    discount_code = djf.CharField(max_length=32, required=False)
    accept_terms_and_conditions = djf.BooleanField()
    if settings.PRODUCTION_MODE:
        captcha = cfields.ReCaptchaField(
            attrs={'theme': 'clean'},
            use_ssl=True)

    def clean_accept_terms_and_conditions(self):
        if not self.cleaned_data.get("accept_terms_and_conditions"):
            raise ValidationError("You must accept the terms and conditions to register")


class ScientificTeamForm(djf.ModelForm):
    class Meta:
        model = models.Team
        exclude = ['owner']


class UserAccountForm(djf.ModelForm):
    class Meta:
        model = models.UserAccount
        exclude = [
            'user',
            'password',
            'last_login',
            'last_connected',
            "date_joined",
            "username",
            "email_address",
            "firstname",
            "lastname",
            "following",
            "groups",
            "user_permissions",
            "scientific_profile",
            "business_profile"]
        fieldgroups = [('basic_info', ['first_name', 'last_name'])
                       ]


class UserAccountFormUser(djf.ModelForm):
    class Meta:
        model = models.UserAccount
        exclude = [
            'user',
            'password',
            'last_login',
            'last_connected',
            "date_joined",
            "username",
            "email_address",
            "firstname",
            "lastname",
            "is_superuser",
            "following",
            "groups",
            "user_permissions",
            "scientific_profile",
            "business_profile"]
        fieldgroups = [('basic_info', ['first_name', 'last_name'])
                       ]
